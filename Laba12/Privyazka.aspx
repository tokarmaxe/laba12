﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Privyazka.aspx.cs" Inherits="Laba12.Privyazka" %>

<script runat="server">
    static Hashtable hshDays;
    void calSelectChange(Object sender, EventArgs e)
    {
        DateTime datDateIn = calDays.SelectedDate;
        if(Page.IsPostBack)
        {
            lblShow.Text = "На этот день назначен: ";
            lblShow.Text += hshDays[datDateIn];
            if (hshDays[datDateIn] == null)
                lblShow.Text = "Ничего не назначено";
            lblShow.Visible = true;
        }
    }
    void Page_Init()
    {
        if(!Page.IsPostBack)
        {
            hshDays = new Hashtable();
            hshDays[Convert.ToDateTime("4/12/2018")] = "Сдача ПЗ  курсовой работы";
            hshDays[Convert.ToDateTime("6/12/2018")] = "2 модульная контрольная работа";
            hshDays[Convert.ToDateTime("11/12/2018")] = "Защита курсовой работы";
            hshDays[Convert.ToDateTime("25/12/2018")] = "Выходной день";
            Session["Diary"] = hshDays;
        }
    }
    void Record(Object sender, EventArgs e)
    {
        DateTime datDateIn = calDays.SelectedDate;
        //hshDays[datDateIn] = TextBox1.Text;
        lblShow.Text = hshDays[datDateIn].ToString();
    }
</script>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h3>Ежедневник</h3>
            Введите дату между 1/12/2018 и 31/12/2018
            <asp:Calendar ID="calDays" runat="server" OnSelectionChanged="calSelectChange" VisibleDate="12/10/2018"></asp:Calendar>
            <br />
            <br />
            <asp:Label ID="lblShow" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
