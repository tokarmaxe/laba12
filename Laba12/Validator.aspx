﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Validator.aspx.cs" Inherits="Laba12.Validator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="DropDownList1"
                AutoPostBack="True"
                runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                runat="server"
                ErrorMessage="Required"
                ControlToValidate="DropDownList1">
            </asp:RequiredFieldValidator>
            <span class="label">Your Email</span><span
                class="label1">(Required)</span>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator
                ID="RegularExpressionValidator1" runat="server"
                ControlToValidate="TextBox1" ErrorMessage="Not a valid Email"
                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-
.]\w+)*"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                runat="server" ControlToValidate="TextBox1"
                ErrorMessage="*"></asp:RequiredFieldValidator>
            </span>
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <asp:RangeValidator ID="RangeValidator1" runat="server"
                ControlToValidate="TextBox2"
                ErrorMessage="Не больше 10 единиц в одни руки"
                MaximumValue="10" MinimumValue="1" Type="Integer">
            </asp:RangeValidator>

            <input type="submit" />
        </div>
    </form>
</body>
</html>
