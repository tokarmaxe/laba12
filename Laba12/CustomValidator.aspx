﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomValidator.aspx.cs" Inherits="Laba12.CustomValidator" %>

<script type="text/javascript">
    function validatePassword(oSrc, args) {
        args.IsValid = (args.Value.length > 5)
    }
</script>

<script runat="server">
    void ServerValidate(object source, ServerValidateEventArgs args)
    {
        string password = args.Value.ToString();
        int len = password.Length;
        args.IsValid = (len > 5);
    }
</script>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox id="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Слишком короткий пароль" ControlToValidate="txtPassword" ClientValidationFunction="validatePassword" OnServerValidate="ServerValidate"></asp:CustomValidator>
        </div>
    </form>
    <input type="submit" name="Button1" value="Button" onclick="javascript: WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("Button1", "true", "false", "false"))" id="Button1" />

</body>
</html>
