﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidatorForm.aspx.cs" Inherits="Laba12.ValidatorForm" %>

<script runat="server">
    protected void Validate_Click(object sender, EventArgs e)
    {
        if(Page.IsValid)
        {
            lblName.Text = "";
            lblAddress.Text = "";
            lblPassword.Text = "";
            input.Visible = false;
            if (txtName.Text != "")
                lblName.Text = "Вы ввели имя: " + txtName.Text;
            if (txtAddress.Text != "")
                lblAddress.Text = "Вы ввели адрес: " + txtAddress.Text;
            if (txtPassword.Text != "")
                lblPassword.Text = "Вы ввели пароль: " + txtPassword.Text + "<br>Спасибо за регистрацию!";
        }
    }
</script>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="input" runat="server">
        <div style="text-align: left">
            <table>
                <tr>
                    <td style="width:100px">
                        Введите имя:
                    </td>
                    <td style="width:100px">
                        <asp:TextBox ID="txtName" runat="server" CausesValidation="true" />
                    </td>
                    <td style="width:100px">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Необходимо ввести имя!" ControlToValidate="txtName" Display="Static"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width:100px">
                        Введите адрес:
                    </td>
                     <td style="width:100px">
                        <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" Rows="5" Wrap="true" />
                    </td>
                </tr>
                <tr>
                    <td style="width:100px">
                        Введите пароль:
                    </td>
                    <td style="width:100px">
                        <asp:TextBox id="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Пароль не должен быть пустым!" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width:100px">
                        Повторите пароль:
                    </td>
                    <td style="width:100px">
                        <asp:TextBox id="txtPassword2" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Пароли должны совпадать" ControlToValidate="txtPassword" ControlToCompare="txtPassword2"></asp:CompareValidator>
                    </td>
                </tr>
            </table>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        </div>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Валидация" OnClick="Validate_Click" />
        <br />
    </form>
    <asp:Label id="lblName" runat="server" /><br />
    <asp:Label ID="lblAddress" runat="server" /><br />
    <asp:Label ID="lblPassword" runat="server"/><br />
</body>
</html>
